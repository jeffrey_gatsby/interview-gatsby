package interview1;

public class Position {
    public String id;
    public String symbol;
    public LongShort longShort;
    public double quantity;

    public Position(String id, String symbol, LongShort longShort, double quantity) {
        this.id = id;
        this.symbol = symbol;
        this.longShort = longShort;
        this.quantity = quantity;
    }
}
