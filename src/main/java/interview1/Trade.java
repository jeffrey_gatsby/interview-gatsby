package interview1;

import java.util.List;

public class Trade {
    public int id;
    public double quantity;
    public List<TradeLeg> legs;

    public Trade(int id, double quantity, List<TradeLeg> legs) {
        this.id = id;
        this.quantity = quantity;
        this.legs = legs;
    }
}
