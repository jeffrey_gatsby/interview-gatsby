# Problem

Given a list of `Position` and a list of `Trade` match up and return list of miss and extra positions.

Position JSON:
{"id": 1234, "symbol":"ACOR1 210416C00001000", "type": "short", "quantity": 12}

Trade JSON:
{"id": 4569, "quantity": 5, "legs":[{"symbol": "ACOR1 210416C00001000" , "type":"long"}]}

# Notes
The description above is purposely vague, so that you will need to flesh out the details with the interviewer.  Please
focus on edge conditions and how to test them.  This is an extremely hard problem and limiting scope is important.

This from a real problem that we solved at Gatsby, and it tests not only coding but requirements gathering.
