package interview1;

public class TradeLeg {
    public String symbol;
    public LongShort longShort;

    public TradeLeg(String symbol, LongShort longShort) {
        this.symbol = symbol;
        this.longShort = longShort;
    }
}
