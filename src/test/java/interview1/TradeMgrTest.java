package interview1;

import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TradeMgrTest {
    public void testHappyPath() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5),
                new Position("OCC1235", "AAPL", LongShort.Long, 2)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1234, 5,  Arrays.asList(new TradeLeg("ACOR1 210416C00001000", LongShort.Long))),
                new Trade(1235, 2,  Arrays.asList(new TradeLeg("AAPL", LongShort.Long)))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertTrue(issues.missing.isEmpty());
        assertTrue(issues.extra.isEmpty());
    }

    public void testFullMissingTrade() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1234, 5,  Arrays.asList(new TradeLeg("ACOR1 210416C00001000", LongShort.Long))),
                new Trade(1235, 2,  Arrays.asList(new TradeLeg("AAPL", LongShort.Long)))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertEquals(1, issues.missing.size());
        MissingTrade missingTrade = issues.missing.get(0);
        assertEquals(2, missingTrade.quantity);
        assertEquals("AAPL", missingTrade.trade.legs.get(0).symbol);
        assertEquals(LongShort.Long, missingTrade.trade.legs.get(0).longShort);
        assertTrue(issues.extra.isEmpty());
    }

    public void testPartialMissingTrade() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5),
                new Position("OCC1235", "AAPL", LongShort.Long, 2)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1234, 5,  Arrays.asList(new TradeLeg("ACOR1 210416C00001000", LongShort.Long))),
                new Trade(1235, 1,  Arrays.asList(new TradeLeg("AAPL", LongShort.Long)))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertEquals(1, issues.missing.size());
        MissingTrade missingTrade = issues.missing.get(0);
        assertEquals(1, missingTrade.quantity);
        assertEquals("AAPL", missingTrade.trade.legs.get(0).symbol);
        assertEquals(LongShort.Long, missingTrade.trade.legs.get(0).longShort);
        assertTrue(issues.extra.isEmpty());
    }

    public void testFullExtraPosition() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5),
                new Position("OCC1235", "AAPL", LongShort.Long, 2)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1234, 5,  Arrays.asList(new TradeLeg("ACOR1 210416C00001000", LongShort.Long)))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertEquals(1, issues.extra.size());
        ExtraPosition extraPosition = issues.extra.get(0);
        assertEquals(2, extraPosition.quantity);
        assertEquals("AAPL", extraPosition.position.symbol);
        assertEquals(LongShort.Long, extraPosition.position.longShort);

        assertTrue(issues.missing.isEmpty());
    }

    public void testPartialExtraPosition() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5),
                new Position("OCC1235", "AAPL", LongShort.Long, 1)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1234, 5,  Arrays.asList(new TradeLeg("ACOR1 210416C00001000", LongShort.Long))),
                new Trade(1234, 2,  Arrays.asList(new TradeLeg("AAPL", LongShort.Long)))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertEquals(1, issues.extra.size());
        ExtraPosition extraPosition = issues.extra.get(0);
        assertEquals(1, extraPosition.quantity);
        assertEquals("AAPL", extraPosition.position.symbol);
        assertEquals(LongShort.Long, extraPosition.position.longShort);

        assertTrue(issues.missing.isEmpty());
    }

    public void testMismatchLongShort() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1235", "AAPL", LongShort.Long, 2)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1235, 2,  Arrays.asList(new TradeLeg("AAPL", LongShort.Short)))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertFalse(issues.missing.isEmpty());
        assertFalse(issues.extra.isEmpty());
    }

    public void testMultiLegHappy() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5),
                new Position("OCC1234", "ACOR1 210416C00003000", LongShort.Short, 5)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1235, 5,  Arrays.asList(
                        new TradeLeg("ACOR1 210416C00001000", LongShort.Long),
                        new TradeLeg("ACOR1 210416C00003000", LongShort.Short)
                ))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertFalse(issues.missing.isEmpty());
        assertFalse(issues.extra.isEmpty());
    }

    public void testMultiLegAndSingleHappy() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 5),
                new Position("OCC1234", "ACOR1 210416C00003000", LongShort.Short, 2)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1235, 2,  Arrays.asList(
                        new TradeLeg("ACOR1 210416C00001000", LongShort.Long),
                        new TradeLeg("ACOR1 210416C00003000", LongShort.Short)
                )),
                new Trade(1235, 3,  Arrays.asList(
                        new TradeLeg("ACOR1 210416C00001000", LongShort.Long)
                ))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        assertFalse(issues.missing.isEmpty());
        assertFalse(issues.extra.isEmpty());
    }

    public void testMultiLegAndSingleMissingExtra() {
        List<Position> positions = Arrays.asList(
                new Position("OCC1234", "ACOR1 210416C00001000", LongShort.Long, 3),
                new Position("OCC1234", "ACOR1 210416C00003000", LongShort.Short, 3)
        );
        List<Trade> trades = Arrays.asList(
                new Trade(1235, 2,  Arrays.asList(
                        new TradeLeg("ACOR1 210416C00001000", LongShort.Long),
                        new TradeLeg("ACOR1 210416C00003000", LongShort.Short)
                )),
                new Trade(1235, 3,  Arrays.asList(
                        new TradeLeg("ACOR1 210416C00001000", LongShort.Long)
                ))
        );
        ReconcileIssues issues = TradeMgr.reconcile(positions, trades);

        //TODO finish test

    }


}